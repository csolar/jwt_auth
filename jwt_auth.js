var moment = require('moment'),
    jwt = require('jwt-simple');

function jwtAuth(issuer_fun, secret, header, expire)
{
  var _is_allowed = issuer_fun,
      _header = header || 'x-access-token',
      //if expire is not set the auth() function will not 
      //check the expiration date of the jwt token - useful
      //for systems that keep the user always logged in
      _expire = expire || false,
      _secret = secret;

  function auth(req, res, next)
  {
    //look in the header for a token
    var token = req.header(_header);
    if(token)
    {
      try
      {
        //decode the token 
        var decoded =  jwt.decode(token, _secret);
        //check expiration
        var expiration = decoded.exp;
        var now = moment().valueOf();
        var then = moment(expiration).valueOf();
        if(!_expire || (then > now))
        {
          //accept the token and check the issuer
          var issuer = decoded.iss;
          if(typeof _is_allowed === 'function')
          {
            //using a deferred semantic when checking the issuer. The 
            //user supplied checking function can call reject() to notify
            //that the issuer could not be authenticated or call resolve()
            //to indicate successful authentication
            _is_allowed(issuer, {
              reject: function(){
                res.status(403).send('failed to authenticate issuer');
              },
              resolve: function(){
                req.jwtauth = decoded;
                return next();
              }
            });
          }
          else
          {
            res.status(403).send('unable to authenticate issuer');
          }
        }
        else
        {
          res.status(403).send('expired jwt token');
        }
      }
      catch(e)
      {
        return next(e);
      }
    }
    else
    {
      res.status(403).send('un-authenticated request');
    }
  }

  return (auth);
}

module.exports = jwtAuth;