JWT_AUTH [1.0.0]
=============

It's an extremely simple middleware for use with [express][1] and [restify][2] to enable authentication using [JSON Web Tokens][3].

Installation
-------------
````
npm install jwt_auth
````

Usage
-------
*jwt_auth* can be used on a per-route basis for those routes that require authenticated access. 

It requires you to supply a function that actually authenticates the issuer. *jwt_auth* will pass your function the *issuer* that was extracted from the token and an object. This object implements a *deferred* semantic so that your issuer-checking function can be asynchronous: if the issuer is legit, you may call the object's `resolve()` function, otherwise, you will call its `reject()` function. 

If the request is legit, *jwt_auth* will put the token data in the request object so that it can be used by other middleware or your application code.

For example in *express*:

      var express = require('express'),
          app = express(),
          jwtAuth = require('jwt_auth'),
          secret = 'secret-used-to-sign-and-verify-jwt-hashes';

      var check_user_from_database = function(id, deferred)
      {
        //find user by id in the database asynchronously
        db.findById(id, function(err, data){
          if(!data)
            //user not found! do not authenticate request!
            deferred.reject();
          else
            //whohoo!!
            deferred.resolve();
        });
      }

      //---- routes ----

      //anybody can access this route
      app.get('/', function(req, res){
        // do something here..
      });

      //only requests with a jwt token in the headers and that
      //are authenticated can access this resource
      app.get('/top_secret', jwtAuth(check_user_from_database, secret), function(req, res){
        // this is how you retrieve the authenticated issuer
        var user_id = req.jwtauth.iss;
        // list all the documents about the alien invasion....
      });

Configuration
--------------
*jwt_auth* needs four pieces of information:

1. the issuer-checking function - required!
2. the secret to use for signing and verifying the tokens - required!
3. the name of the header where the token is to be found - optional, it defaults to `x-access-token`
4. an optional boolean indicating whether or not to perform a token expiration check. If not specified, *jwt_auth* default is to not perform the expiry check.


[1]: http://expressjs.com/
[2]: http://mcavage.me/node-restify/
[3]: http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-25